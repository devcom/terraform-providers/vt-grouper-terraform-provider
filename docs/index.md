
# VT Grouper Provider

The VT Grouper provider is used to interact with the Virginia Tech Grouper Web Services REST API. [Additional documentation about the VT Grouper web services can be found here.](https://spaces.at.internet2.edu/display/Grouper/Grouper+Web+Services)

Authentication is required to connect to the Grouper web services. As of right now, this provider only supports using a local entity credential.

Documentation for [creating a local etity can be found here](https://spaces.at.internet2.edu/display/Grouper/Grouper+web+services+-+authentication+-+self-service+JWT).

Once created, you will have a `jwt_user` and a private key. Convert the private key to pem:

```
base64 -d ./jwt.key.b64 > jwt.key
openssl rsa -in ./jwt.key -out ./jwt.pem
```

You will also need to grant the local entity admin access to the highest level Grouper (e.g. `dpt:nis`) folder that you want it to manage.

## Example Usage

```hcl
# Configure the VTGrouper provider
provider "vtgrouper" {
  service_auth = {
    jwt_user = "jwtUser_NzFmZWZmNDcxMDQwNDRlMTk5YjgxZmVjMjVlOGEyZTk"
    jwt_key = "/path/to/jwt.pem"
  }
}

resource "vtgrouper_folder" "dpt-es-org-tf-testing" {
  name = "dpt:es:org:tf-testing"
}

resource "vtgrouper_group" "test-group" {
  name = "test-group"
  folder = vtgrouper_folder.dpt-es-org-tf-testing.name
  # Members can be people or other groups
  members = [
    "dpt:es:org:dbaa",
    "jaylor"
  ]
}
``` 

## Argument Reference

The following arguments are supported in the `provider` block:

- `service_auth` (Required) - A [service auth](#service-auth-arguments) block that configures JWT

### Service Auth Arguments

All files for service authentication should be PEM-encoded. A passphrase for the private key is currently not supported.

- `jwt_user` (Required) - The JWT User established for your local entity
- `jwt_key` (Required) - The file path to the JWT key in pem format

