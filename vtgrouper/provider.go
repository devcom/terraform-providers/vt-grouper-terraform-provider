package vtgrouper

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
)

type ProviderConfig struct {
	client    *GrouperClient
	serviceId string
}

func Provider() terraform.ResourceProvider {
	p := &schema.Provider{

		Schema: map[string]*schema.Schema{
			"base_url": {
				Type:        schema.TypeString,
				Optional:    true,
				Default:     "https://grouper.it.vt.edu/grouper-ws/servicesRest/json/v2_6_000",
				Description: "Base url for API requests",
			},
			"service_auth": {
				Type:        schema.TypeMap,
				Description: "Auth details to connect to grouper",
				Required:    true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						// To convert jwt key that Grouper provides to pem format:
						// base64 -d ./jwt.key.b64 > jwt.key
						// openssl rsa -in ./jwt.key -out ./jwt.pem
						"jwt_user": {
							Type:        schema.TypeString,
							Optional:    false,
							Description: "JWT user provided by grouper. ( Eg. jwtUser_YkopZWZmNDcxMkicdDRlMTk5YjgxZmVjMjVlOGEabcd )",
						},
						"jwt_key": {
							Type:        schema.TypeString,
							Optional:    false,
							Description: "Path to file containing JWT Key in PEM format",
						},
					},
				},
			},
		},

		DataSourcesMap: map[string]*schema.Resource{},

		ResourcesMap: map[string]*schema.Resource{
			"vtgrouper_group":  resourceVtGrouperGroup(),
			"vtgrouper_folder": resourceVtGrouperFolder(),
		},

		ConfigureFunc: func(d *schema.ResourceData) (interface{}, error) {
			serviceAuthConfig := d.Get("service_auth").(map[string]interface{})

			client := NewGrouperClient(
				d.Get("base_url").(string),
				serviceAuthConfig["jwt_user"].(string),
				serviceAuthConfig["jwt_key"].(string),
			)

			providerConfig := new(ProviderConfig)
			providerConfig.client = client

			return providerConfig, nil
		},
	}

	return p
}
