package vtgrouper

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/tidwall/gjson"
)

func encodeGroup(group string) string {
	return strings.ReplaceAll(group, ":", "%3A")
}

func splitGroup(group string) (string, string) {
	groupComponents := strings.Split(group, ":")
	groupName := groupComponents[len(groupComponents)-1]
	folder := strings.Join(groupComponents[:len(groupComponents)-1], ":")
	return folder, groupName
}

func resourceVtGrouperGroup() *schema.Resource {
	return &schema.Resource{
		Create:   resourceVtGrouperGroupCreate,
		Read:     resourceVtGrouperGroupRead,
		Update:   resourceVtGrouperGroupUpdate,
		Delete:   resourceVtGrouperGroupDelete,
		Importer: resourceVtGrouperGroupImporter(),

		Schema: map[string]*schema.Schema{
			"folder": &schema.Schema{
				Type:        schema.TypeString,
				Description: "The name of the folder containing the group",
				Required:    true,
				ForceNew:    true,
			},
			"name": &schema.Schema{
				Type:        schema.TypeString,
				Description: "The name of the group",
				Required:    true,
				ForceNew:    true,
			},
			"members": &schema.Schema{
				Type:        schema.TypeSet,
				Description: "Membership of the group",
				Optional:    true,
				Set:         schema.HashString,
				Elem:        &schema.Schema{Type: schema.TypeString},
			},
			"is_composite": &schema.Schema{
				Type:        schema.TypeBool,
				Description: "Whether this is a composite group",
				Optional:    true,
				Default:     false,
				ForceNew:    true,
			},
			"composite_type": &schema.Schema{
				Type:        schema.TypeString,
				Description: "The type of composite group. One of ['complement', '???']",
				Optional:    true,
			},
			"composite_left": &schema.Schema{
				Type:        schema.TypeString,
				Description: "Left side of the composite",
				Optional:    true,
			},
			"composite_right": &schema.Schema{
				Type:        schema.TypeString,
				Description: "Right side of the composite",
				Optional:    true,
			},
		},
	}
}

func resourceVtGrouperGroupCreate(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	grouperClient := providerConfig.client

	groupName := d.Get("name").(string)
	folder := d.Get("folder").(string)
	group := fmt.Sprintf("%s:%s", folder, groupName)

	type GroupSaveRequest struct {
		GroupName string `json:"groupName"`
	}
	type Query struct {
		WsRestGroupSaveLiteRequest GroupSaveRequest
	}
	query := &Query{
		WsRestGroupSaveLiteRequest: GroupSaveRequest{
			GroupName: group}}
	jsonQuery, _ := json.Marshal(query)
	_, err := grouperClient.Post(fmt.Sprintf("/groups/%s", encodeGroup(group)), string(jsonQuery))
	if err != nil {
		return err
	}
	d.SetId(group)

	err = applyGroupPatchesWhereNecessary(grouperClient, d)
	if err != nil {
		return err
	}

	return resourceVtGrouperGroupRead(d, meta)
}

func resourceVtGrouperGroupRead(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	client := providerConfig.client

	folder, group := splitGroup(d.Id())

	// Fetch the group
	type FindGroupsRequest struct {
		QueryFilterType    string `json:"queryFilterType"`
		GroupName          string `json:"groupName"`
		IncludeGroupDetail string `json:"includeGroupDetail"`
	}
	type GroupQuery struct {
		WsRestFindGroupsLiteRequest FindGroupsRequest
	}
	groupQuery := &GroupQuery{
		WsRestFindGroupsLiteRequest: FindGroupsRequest{
			QueryFilterType:    "FIND_BY_GROUP_NAME_EXACT",
	        GroupName: d.Id(),
			IncludeGroupDetail: "T"}}
	jsonQuery, err := json.Marshal(groupQuery)

	groupInfo, err := client.Post("/groups", string(jsonQuery))
	if err != nil {
		return err
	}
	// Do some error checking
	resultCode := groupInfo.Get("WsFindGroupsResults").Get("resultMetadata").Get("resultCode").String()
	if resultCode != "SUCCESS" {
		return errors.New(fmt.Sprintf("Error reading group: %s",
			groupInfo.Get("WsFindGroupsResults").Get("resultMetadata").Get("resultMessage").String()))
	}
	// We expect only one result, if we get more, we need to figure out which one to use
	groupIndex := 0
	if len(groupInfo.Get("WsFindGroupsResults").Get("groupResults").Array()) > 1 {
		for i, g := range groupInfo.Get("WsFindGroupsResults").Get("groupResults").Array() {
			if g.Get("name").String() == group {
				groupIndex = i
				break
			}
		}
	}
	groupJson := groupInfo.Get("WsFindGroupsResults").Get("groupResults").Array()[groupIndex]
	folder, group = splitGroup(groupJson.Get("name").String())
	d.Set("name", group)
	d.Set("folder", folder)
	d.Set("id", fmt.Sprintf("%s:%s", folder, group))

	if groupJson.Get("detail").Get("hasComposite").String() == "T" {
		d.Set("is_composite", true)
		d.Set("composite_type", groupJson.Get("detail").Get("compositeType").String())
		d.Set("composite_left", groupJson.Get("detail").Get("leftGroup").Get("name").String())
		d.Set("composite_right", groupJson.Get("detail").Get("rightGroup").Get("name").String())
	} else {
		// Fetch members
		type GroupNames struct {
			GroupName string `json:"groupName"`
		}
		type GetMembersRequest struct {
			WsGroupLookups        []GroupNames `json:"wsGroupLookups"`
			SubjectAttributeNames []string     `json:"subjectAttributeNames"`
			MemberFilter          string       `json:"memberFilter"`
		}
		type MemberQuery struct {
			WsRestGetMembersRequest GetMembersRequest
		}
		memberQuery := &MemberQuery{WsRestGetMembersRequest: GetMembersRequest{
			WsGroupLookups:        []GroupNames{{GroupName: d.Id()}},
			SubjectAttributeNames: []string{"uupid"},
			MemberFilter:          "Immediate"}}
		jsonMemberQuery, _ := json.Marshal(memberQuery)
		result, err := client.Post("/groups", string(jsonMemberQuery))
		// Do some error checking
		if err != nil {
			return err
		}
		resultCode = result.Get("WsGetMembersResults").Get("resultMetadata").Get("resultCode").String()
		if resultCode != "SUCCESS" {
			return errors.New(fmt.Sprintf("Error reading member list: %s",
				result.Get("WsGetMembersResults").Get("resultMetadata").Get("resultMessage").String()))
		}

		memberInfo := result.Get("WsGetMembersResults").Get("results").Array()[0].Get("wsSubjects").Array()
		members := []string{}
		for i := 0; i < len(memberInfo); i++ {
			source := memberInfo[i].Get("sourceId").String()
			if source == "ed" {
				uupid := memberInfo[i].Get("attributeValues").Array()[0].String()
				members = append(members, uupid)
			} else if source == "g:gsa" {
				group := memberInfo[i].Get("name").String()
				members = append(members, group)
			} else {
				return errors.New(fmt.Sprintf("Unknown group source: %s", memberInfo[i]))
			}
		}
		d.Set("members", members)
	}
	return nil
}

func resourceVtGrouperGroupUpdate(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	client := providerConfig.client

	err := applyGroupPatchesWhereNecessary(client, d)
	if err != nil {
		return err
	}

	return resourceVtGrouperGroupRead(d, meta)
}

func resourceVtGrouperGroupDelete(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	client := providerConfig.client

	groupName := d.Get("name").(string)
	folder := d.Get("folder").(string)
	group := fmt.Sprintf("%s:%s", folder, groupName)

	_, err := client.Delete("/groups/" + encodeGroup(group))
	if err != nil {
		return err
	}

	return nil
}

func resourceVtGrouperGroupImporter() *schema.ResourceImporter {
	return &schema.ResourceImporter{
		State: func(data *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
			err := resourceVtGrouperGroupRead(data, meta)
			if err != nil {
				return nil, err
			}
			return []*schema.ResourceData{data}, nil
		},
	}
}

func applyGroupPatchesWhereNecessary(grouperClient *GrouperClient, d *schema.ResourceData) error {
	groupName := d.Get("name").(string)
	folder := d.Get("folder").(string)
	group := fmt.Sprintf("%s:%s", folder, groupName)

	if d.HasChange("members") {
		o, n := d.GetChange("members")
		syncGrouperGroupMembers(group, n.(*schema.Set), o.(*schema.Set), grouperClient)
	}

	return nil
}

func findMember(client *GrouperClient, name string) gjson.Result {
	type SearchString struct {
		SearchString string `json:"searchString"`
	}
	type Query struct {
		WsRestGetSubjectsLiteRequest SearchString
	}
	query := &Query{
		WsRestGetSubjectsLiteRequest: SearchString{
			SearchString: name}}
	jsonQuery, _ := json.Marshal(query)
	result, err := client.Post("/subjects", string(jsonQuery))
	// Do some error checking
	if err != nil {
		panic(err)
	}
	resultCode := result.Get("WsGetSubjectsResults").Get("resultMetadata").Get("resultCode").String()
	if resultCode != "SUCCESS" {
		panic(errors.New(fmt.Sprintf("Error finding member: %s",
			result.Get("WsGetSubjectsResults").Get("resultMetadata").Get("resultMessage").String())))
	}
	// We expect only one result
	subjectIndex := 0
	if len(result.Get("WsGetSubjectsResults").Get("wsSubjects").Array()) > 1 {
		for i, g := range result.Get("WsGetSubjectsResults").Get("wsSubjects").Array() {
			if g.Get("name").String() == name {
				subjectIndex = i
				break
			}
		}
	}
	return result.Get("WsGetSubjectsResults").Get("wsSubjects").Array()[subjectIndex]

}

func syncGrouperGroupMembers(group string, newMembers *schema.Set, oldMembers *schema.Set, client *GrouperClient) error {
	var targetsToAdd []interface{}
	if oldMembers == nil {
		targetsToAdd = newMembers.List()
	} else {
		targetsToAdd = newMembers.Difference(oldMembers).List()
	}
	for _, n := range targetsToAdd {
		member := findMember(client, n.(string))
		client.Put(fmt.Sprintf("/groups/%s/members/%s", encodeGroup(group), member.Get("id").String()), "")
	}

	var targetsToRemove []interface{}
	if oldMembers == nil {
		targetsToRemove = []interface{}{}
	} else {
		targetsToRemove = oldMembers.Difference(newMembers).List()
	}
	for _, o := range targetsToRemove {
		member := findMember(client, o.(string))
		client.Delete(fmt.Sprintf("/groups/%s/members/%s", encodeGroup(group), member.Get("id").String()))
	}

	return nil
}
