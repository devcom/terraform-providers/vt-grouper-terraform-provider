package vtgrouper

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

func resourceVtGrouperFolder() *schema.Resource {
	return &schema.Resource{
		Create:   resourceVtGrouperFolderCreate,
		Read:     resourceVtGrouperFolderRead,
		Delete:   resourceVtGrouperFolderDelete,
		Importer: resourceVtGrouperFolderImporter(),

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:        schema.TypeString,
				Description: "The name of the group",
				Required:    true,
				ForceNew:    true,
			},
		},
	}
}

func resourceVtGrouperFolderCreate(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	grouperClient := providerConfig.client

	folder := d.Get("name").(string)

	type StemSaveRequest struct {
		StemName string `json:"stemName"`
	}
	type Query struct {
		WsRestStemSaveLiteRequest StemSaveRequest
	}
	query := &Query{
		WsRestStemSaveLiteRequest: StemSaveRequest{
			StemName: folder}}
	jsonQuery, _ := json.Marshal(query)
	_, err := grouperClient.Post(fmt.Sprintf("/stems/%s", encodeGroup(folder)), string(jsonQuery))
	if err != nil {
		return err
	}
	d.SetId(folder)

	return resourceVtGrouperFolderRead(d, meta)
}

func resourceVtGrouperFolderRead(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	client := providerConfig.client

	folder := d.Id()

	// Fetch the group
	type FindStemRequest struct {
		StemName            string `json:"stemName"`
		StemQueryFilterType string `json:"stemQueryFilterType"`
	}
	type StemQuery struct {
		WsRestFindStemsLiteRequest FindStemRequest
	}
	stemQuery := &StemQuery{
		WsRestFindStemsLiteRequest: FindStemRequest{
			StemName:            folder,
			StemQueryFilterType: "FIND_BY_STEM_NAME_APPROXIMATE"}}
	jsonQuery, err := json.Marshal(stemQuery)

	groupInfo, err := client.Post("/stems", string(jsonQuery))
	if err != nil {
		return err
	}
	// Do some error checking
	resultCode := groupInfo.Get("WsFindStemsResults").Get("resultMetadata").Get("resultCode").String()
	if resultCode != "SUCCESS" {
		return errors.New(fmt.Sprintf("Error reading group: %s",
			groupInfo.Get("WsFindStemsResults").Get("resultMetadata").Get("resultMessage").String()))
	}
	// We expect only one result, if we get more, we need to figure out which one to use
	stemIndex := 0
	if len(groupInfo.Get("WsFindStemsResults").Get("stemResults").Array()) > 1 {
		for i, g := range groupInfo.Get("WsFindStemsResults").Get("stemResults").Array() {
			if g.Get("name").String() == folder {
				stemIndex = i
				break
			}
		}
	}
	stem := groupInfo.Get("WsFindStemsResults").Get("stemResults").Array()[stemIndex].Get("name").String()
	d.Set("name", stem)
	d.Set("id", stem)

	return nil
}

func resourceVtGrouperFolderDelete(d *schema.ResourceData, meta interface{}) error {
	providerConfig := meta.(*ProviderConfig)
	client := providerConfig.client

	folder := d.Get("name").(string)

	_, err := client.Delete("/stems/" + encodeGroup(folder))
	if err != nil {
		return err
	}

	return nil
}

func resourceVtGrouperFolderImporter() *schema.ResourceImporter {
	return &schema.ResourceImporter{
		State: func(data *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
			err := resourceVtGrouperFolderRead(data, meta)
			if err != nil {
				return nil, err
			}
			return []*schema.ResourceData{data}, nil
		},
	}
}
