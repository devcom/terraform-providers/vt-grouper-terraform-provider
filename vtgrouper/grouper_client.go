package vtgrouper

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"

	"github.com/tidwall/gjson"
)

var httpClient *http.Client

type GrouperClient struct {
	baseUrl    string
	httpClient *http.Client
	authHeader string
}

func NewGrouperClient(baseUrl string, jwtUser string, jwtKey string) *GrouperClient {
	client := new(GrouperClient)
	client.baseUrl = baseUrl

	// http://www.inanzzz.com/index.php/post/kdl9/creating-and-validating-a-jwt-rsa-token-in-golang
	key, _ := ioutil.ReadFile(jwtKey)
	parsedKey, err := jwt.ParseRSAPrivateKeyFromPEM(key)
	if err != nil {
		panic(err)
	}

	now := time.Now()
	claims := make(jwt.MapClaims)
	claims["exp"] = now.Add(time.Minute * 10).Unix() // The expiration time after which the token must be disregarded.
	claims["iat"] = now.Unix()                       // The time at which the token was issued.
	claims["nbf"] = now.Unix()                       // The time before which the token must be disregarded.

	token, err := jwt.NewWithClaims(jwt.SigningMethodRS256, claims).SignedString(parsedKey)
	if err != nil {
		panic(err)
	}
	client.authHeader = fmt.Sprintf("Bearer %s=_%s", jwtUser, token)
	client.httpClient = &http.Client{}

	return client
}

func (e *GrouperClient) Get(url string) (*gjson.Result, error) {
	log.Printf("[DEBUG] GET %s", url)
	req, err := http.NewRequest(http.MethodGet, e.baseUrl+url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", e.authHeader)
	resp, err := e.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	data, err := handleResponse(resp)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (e *GrouperClient) Post(url string, body string) (*gjson.Result, error) {
	log.Printf("[DEBUG] POST %s Body: %s", url, body)
	req, err := http.NewRequest(http.MethodPost, e.baseUrl+url, strings.NewReader(body))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", e.authHeader)
	req.Header.Set("Content-Type", "application/json")
	log.Printf("[DEBUG] POST Headers: %s", req.Header)
	resp, err := e.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	data, err := handleResponse(resp)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (e *GrouperClient) Put(url string, body string) (*gjson.Result, error) {
	log.Printf("[DEBUG] PUT %s Body: %s", url, body)
	req, err := http.NewRequest(http.MethodPut, e.baseUrl+url, strings.NewReader(body))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", e.authHeader)
	resp, err := e.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	data, err := handleResponse(resp)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (e *GrouperClient) Delete(url string) (*gjson.Result, error) {
	log.Printf("[DEBUG] DELETE %s", url)
	req, err := http.NewRequest(http.MethodDelete, e.baseUrl+url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", e.authHeader)
	resp, err := e.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	data, err := handleResponse(resp)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (e *GrouperClient) Patch(url string, patch string) (*gjson.Result, error) {
	log.Printf("[DEBUG] PATCH %s Body: %s", url, patch)
	req, err := http.NewRequest(http.MethodPatch, e.baseUrl+url, strings.NewReader(patch))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", e.authHeader)
	resp, err := e.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	data, err := handleResponse(resp)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func handleResponse(response *http.Response) (*gjson.Result, error) {
	json, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	if response.StatusCode != 200 && response.StatusCode != 201 && response.StatusCode != 204 {
		log.Printf("[WARN] received status code %d from server with response: %s", response.StatusCode, string(json))
		return nil, errors.New(fmt.Sprintf("received error: %s", string(json)))
	}

	jsonData := gjson.ParseBytes(json)
	log.Printf("[DEBUG] response data: %s", jsonData)
	return &jsonData, nil
}
