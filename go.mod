module github.com/terraform-provider-vt-grouper-group

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/hashicorp/terraform-plugin-sdk v1.13.1
	github.com/tidwall/gjson v1.6.0
)
