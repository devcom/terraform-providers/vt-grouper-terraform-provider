# VT Grouper Terraform Provider

This project provides the ability to manage Grouper groups using Terraform.


## Installation

To use the provider, you need the plugin binary installed where Terraform will look for it. Since GitLab releases doesn't yet support the ability to attach binaries to releases, we are building container images that contain the latest build.

1. [Look up the latest tag](https://code.vt.edu/devcom/terraform-providers/vt-grouper-terraform-provider/-/tags). You're also welcome to use `main` to get the latest build.

    ```bash
    IMAGE_TAG=main
    ```

1. Pull the container image and extract the build artifacts from the image into a local `build` directory.
  
    ```bash
    mkdir -p build
    CONTAINER_ID=$(docker create code.vt.edu:5005/devcom/terraform-providers/vt-grouper-terraform-provider:$IMAGE_TAG)
    docker cp $CONTAINER_ID:/ ./build
    docker rm $CONTAINER_ID
    ```   

1. Identify the binary that matches your OS and architecture in the build directory.   
1. Move that binary into the `~/.terraform.d/plugins` directory. Feel free to delete all other builds.
1. Remove the `-OS-arch` suffix from the binary. Its final name should be `terraform-provider-vtgrouper`.
1. Write your Terraform configuration.


## Documentation

[View documentation here](./docs)

All provider docs (including resources and data sources) are found in the `docs` folder. Feel free to submit a MR for changes or clarifications.


## License

[Per the Terraform provider documentation](https://www.terraform.io/guides/terraform-provider-development-program.html#3-development-amp-test), "all Terraform providers are required to contain a MPL-2.0 open source license." Therefore, this provider is leveraging the MPL-2.0 license.

